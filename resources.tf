# defining providers
provider "aws" {
	access_key = var.access_key
	secret_key = var.secret_key
	region = var.region
}

# creating vpc
resource "aws_vpc" "vpc" {
  cidr_block = var.cidr_vpc
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    env = var.environment_tag
  }
}

# creating internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    env = var.environment_tag
  }
}

# creating public subnet
resource "aws_subnet" "subnet_public" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.cidr_subnet
  map_public_ip_on_launch = "true"
  availability_zone = var.availability_zone
  tags = {
    env = var.environment_tag
  }
}

# creating route table
resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    env = var.environment_tag
  }
}

# associating route table with subnet
resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = aws_subnet.subnet_public.id
  route_table_id = aws_route_table.rtb_public.id
}

# creating security group
resource "aws_security_group" "sg_22_80" {
  name = "sg_22"
  vpc_id = aws_vpc.vpc.id

  # allowing SSH access
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  # allowing Http access
  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  # -1 everything
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    env = var.environment_tag
  }
}

# creating the public key to communicate with the ec2 instance
resource "aws_key_pair" "ec2key" {
  key_name = "publicKey"
  public_key = file(var.public_key_path)
}


# creating ec2 instance
resource "aws_instance" "testInstance" {
  ami           = var.instance_ami
  instance_type = var.instance_type
  subnet_id = aws_subnet.subnet_public.id
  vpc_security_group_ids = [aws_security_group.sg_22_80.id]
  key_name = aws_key_pair.ec2key.key_name
  tags = {
		env = var.environment_tag
	}


  # installing apache, mysql and php on the just created ec2 instance
  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /var/www/html/",
      "sudo yum update -y",
      "sudo yum install -y httpd",
      "sudo service httpd start",
      "sudo usermod -a -G apache centos",
      "sudo chown -R centos:apache /var/www",
      "sudo yum install -y mysql php php-mysql",
    ]
  }

  #copy the index.html file form local to the remote machine
  provisioner "file" { 
    source      = "index.html"
    destination = "/tmp/index.html"
  }
  provisioner "file" { 
    source      = "aws_migration.png"
    destination = "/tmp/aws_migration.png"
  }
  provisioner "remote-exec" {
	inline = [
	   "sudo mv /tmp/index.html /var/www/html/index.html",
     "sudo mv /tmp/aws_migration.png /var/www/html/aws_migration.png",
	   ]
	}

  # connecting via ssh to the just created ec2 instance
  connection {
    type     = "ssh"
    host     = self.public_ip
    user     = "ec2-user"
    password = ""
    private_key = file("id_rsa")
  }
}

# create a hosted zone in Route 53 with domain jangomart.tk
# jangomart.tk is hosted on https://my.freenom.com/
resource "aws_route53_zone" "zangomart_zone" {
  name = "jangomart.tk" 
}

# generate the list of name servers and update all those on freenom.com 
output "name_server" {
  value = aws_route53_zone.zangomart_zone.name_servers
}

#request a certificate using the aws_acm_certificate resource
resource "aws_acm_certificate" "zangomart_cert_request" {
    domain_name = "jangomart.tk"
    subject_alternative_names = [ "*.jangomart.tk" ]
    validation_method = "DNS"
    tags = {
      "Name" = "jangomart.tk"
    }
    lifecycle {
        # when the time comes to renew your certificate, it should create the new certificate before deleting the old one
        create_before_destroy = true
    }
}

#the CNAME information returned from the certificate request, and a time-to-live for your CNAME record
resource "aws_route53_record" "zangomart_validation_record" {
    zone_id = aws_route53_zone.zangomart_zone.zone_id
    name = "www.jangomart.tk"
    type = "A"
    records = [var.eip]
    ttl = "300"
}